import java.util.*;

public class Classskills {

	String skill1Name = "Basic weapon Attack";
	String skill2Name, skill3Name, skill4Name;
	int skill1Dmg, skill2Dmg, skill3Dmg, skill4Dmg;
	int skill1CD, skill2CD, skill3CD, skill4CD;
	int skill1ACD = 0, skill2ACD = 0, skill3ACD = 0, skill4ACD = 0;
	String statusEffectName;
	int statusEffectDmg;
	Random rand = new Random();

	Classskills(int class_input) {

		int ci = class_input;

		switch (ci) {

		case 1: // Fighter
			this.skill1Name = skill1Name + " (3-5 DMG + Weapon) \t";
			this.skill1Dmg = this.getDice(1, 3) + 2;
			this.skill1CD = 0;
			this.statusEffectName = "stun";

			this.skill2Name = "CHARGE! (3 - 10 DMG + Weapon) \t \t";
			this.skill2Dmg = this.getDice(1, 8) + 2;
			this.skill2CD = 3;

			this.skill3Name = "Slice (3 - 6 DMG + Weapon) \t \t";
			this.skill3Dmg = this.getDice(1, 5) + 2;
			this.skill3CD = 2;

			this.skill4Name = "Throw Javelin (3 - 8 DMG + Weapon) \t";
			this.skill4Dmg = this.getDice(1, 6) + 2;
			this.skill4CD = 3;
			break;

		case 2: // Mage
			this.skill1Name = skill1Name + " (1 - 3 DMG + Weapon) \t";
			this.skill1Dmg = this.getDice(1, 3);
			this.skill1CD = 0;
			this.statusEffectName = "burn";
			this.statusEffectDmg = this.getDice(2, 4);

			this.skill2Name = "Fireball (7 - 22 DMG + Weapon) \t \t \t";
			this.skill2Dmg = this.getDice(3, 6) + 4;
			this.skill2CD = 4;

			this.skill3Name = "Magic Missile (5 - 11 DMG + Weapon) \t \t";
			this.skill3Dmg = this.getDice(3, 3) + 2;
			this.skill3CD = 2;

			this.skill4Name = "Lightning Strike (6 - 18 DMG + Weapon) \t \t";
			this.skill4Dmg = this.getDice(4, 4) + 2;
			this.skill4CD = 3;
			break;

		case 3: // Rogue
			this.skill1Name = skill1Name + " (2 - 4 DMG + Weapon) \t";
			this.skill1Dmg = this.getDice(1, 3) + 1;
			this.skill1CD = 0;
			this.statusEffectName = "poison";
			this.statusEffectDmg = this.getDice(1, 4);

			this.skill2Name = "Throw Daggers (4 - 12 DMG + Weapon) \t \t";
			this.skill2Dmg = this.getDice(2, 5) + 2;
			this.skill2CD = 3;

			this.skill3Name = "Slit (3 - 10 DMG + Weapon) \t \t \t";
			this.skill3Dmg = this.getDice(2, 4) + 2;
			this.skill3CD = 2;

			this.skill4Name = "Poison Bomb (3 - 10 DMG + Weapon) \t \t";
			this.skill4Dmg = this.getDice(2, 3) + 1;
			this.skill4CD = 2;
			break;

		case 4: // Ranger
			this.skill1Name = skill1Name + " (1 - 3 DMG + Weapon) \t";
			this.skill1Dmg = this.getDice(1, 3);
			this.skill1CD = 0;
			this.statusEffectName = "mark";

			this.skill2Name = "Shoot Arrow (3 - 7 DMG + Weapon) \t \t";
			this.skill2Dmg = this.getDice(1, 5) + 2;
			this.skill2CD = 2;

			this.skill3Name = "Power Shot (6 - 12 DMG + Weapon) \t \t";
			this.skill3CD = 3;
			this.skill3Dmg = this.getDice(2, 4) + 4;

			this.skill4Name = "Arrow Rain (4 - 17 DMG + Weapon) \t \t";
			this.skill4CD = 4;
			this.skill4Dmg = this.getDice(4, 4) + 1;
			break;

		default:
			break;
		}

	}

	// Output Skills
	public void getSkills() {
		System.out.println("1.) CD:(" + this.skill1ACD + ")  " + this.skill1Name);
		System.out.println("2.) CD:(" + this.skill2ACD + ")  " + this.skill2Name);
		System.out.println("3.) CD:(" + this.skill3ACD + ")  " + this.skill3Name);
		System.out.println("4.) CD:(" + this.skill4ACD + ")  " + this.skill4Name);
	}

	public int damageSkill1() {
		return this.skill1Dmg;
	}

	public int damageSkill2() {
		return this.skill2Dmg;
	}

	public int damageSkill3() {
		return this.skill3Dmg;
	}

	public int damageSkill4() {
		return this.skill4Dmg;
	}

	public void decreaseCD() {
		if (this.skill2ACD > 0) {
			this.skill2ACD--;
		}
		if (this.skill3ACD > 0) {
			this.skill3ACD--;
		}
		if (this.skill4ACD > 0) {
			this.skill4ACD--;
		}
	}

	public void increaseCDSkill2() {
		this.skill2ACD = this.skill2CD;
	}

	public void increaseCDSkill3() {
		this.skill3ACD = this.skill3CD;
	}

	public void increaseCDSkill4() {
		this.skill4ACD = this.skill4CD;
	}

	public void resetAllCD() {
		this.skill2ACD = 0;
		this.skill3ACD = 0;
		this.skill4ACD = 0;
	}

	// Dice Roller
	public int getDice(int numberOfDice, int sidesOfDie) {
		int roll = 0;
		for (int counter = 1; counter <= numberOfDice; counter++) {
			roll = roll + rand.nextInt(sidesOfDie) + 1;
		}
		return roll;
	}
}