import java.util.*;

class Armor {
	
	Random rand = new Random();
	String type;
	int worth = 0;
	String usableClass;
	int ac = 0;
	String name;
	
	// Armor constructor
	Armor() {
		int r = this.getDice(1, 4);
		
		switch (r) {
		
		case 1:
			this.type = "Robe";
			this.usableClass = "Mage";
			break;
			
		case 2:
			this.type = "Leather Armor";
			this.usableClass = "Rogue";
			break;
			
		case 3:
			this.type = "Hide Armor";
			this.usableClass = "Ranger";
			break;
			
		case 4:
			this.type = "Plate Armor";
			this.usableClass = "Fighter";
			break;
		
		default:
			this.type = "Snow";
			this.usableClass = "Snow";
			break;
		}
		
		r = this.getDice(1, 20);
		switch (r) {
		
		case 1:
		case 2:
			this.name = "Thunderbird's " + this.type;
			this.ac = this.ac + 1;
			break;
			
		case 3:
			this.name = this.type + " of DOOM!";
			this.ac = this.ac + 5;
			break;
			
		case 4:			
		case 5:
			this.name = "Firebird's " + this.type;
			this.ac = this.ac + 2;
			break;
			
		case 6:
		case 7:
			this.name = "Icebird's " + this.type;
			this.ac = this.ac + 3;
			
		default:
			this.name = "Poor " + this.type;
		}
	}
	
	// Armor methods
	
	public int rollArmorWorth(int charLVL) {
		return this.worth + this.getDice(charLVL, charLVL);
	}
	
	public int rollArmorAC(int charLVL) {
		if (charLVL <= 4) {
		return this.ac + this.getDice(1, charLVL);
		} else {
			return this.ac + this.getDice(1, 4);
		}
	}
	
	// Dice Roller
		public int getDice(int numberOfDice, int sidesOfDie) {
			int roll = 0;
			for (int counter = 1; counter <= numberOfDice; counter++) {
				roll = roll + rand.nextInt(sidesOfDie) + 1;
			}
			return roll;
		}
}