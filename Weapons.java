import java.util.*;

class Weapons {

	Random rand = new Random();
	String type;
	int worth = 0;
	String usableClass;
	int dmg = 1;
	int acc = 1;
	String name;

	// Weapon constructor
	Weapons() {
		int r = this.getDice(1, 12);

		switch (r) {

		case 1:
			this.type = "Axe";
			this.usableClass = "Fighter";
			this.worth = this.worth + 6;
			break;

		case 2:
			this.type = "Sword";
			this.usableClass = "Fighter";
			this.worth = this.worth + 5;
			break;

		case 3:
			this.type = "Dagger";
			this.usableClass = "Rogue";
			this.worth = this.worth + 15;
			break;

		case 4:
			this.type = "Bow";
			this.usableClass = "Ranger";
			this.worth = this.worth + 15;
			break;

		case 5:
			this.type = "Maul";
			this.usableClass = "Fighter";
			this.worth = this.worth + 6;
			break;

		case 6:
			this.type = "Quarterstaff";
			this.usableClass = "Mage";
			this.worth = this.worth + 15;
			break;

		case 7:
			this.type = "Crossbow";
			this.usableClass = "Rogue";
			this.worth = this.worth + 12;
			break;

		case 8:
			this.type = "Club";
			this.usableClass = "Fighter";
			this.worth = this.worth + 2;
			break;

		case 9:
			this.type = "Morningstar";
			this.usableClass = "Fighter";
			this.worth = this.worth + 7;
			break;

		case 10:
			this.type = "Spear";
			this.usableClass = "Fighter";
			this.worth = this.worth + 3;
			break;

		case 11:
			this.type = "Sling";
			this.usableClass = "Ranger";
			this.worth = this.worth + 6;
			break;

		case 12:
			this.type = "Wand";
			this.usableClass = "Mage";
			this.worth = this.worth + 15;
			break;

		default:
			this.type = "Snow";
			this.usableClass = "Snow";
			this.worth = 0;
			break;
		}

		r = this.getDice(1, 12);
		switch (r) {

		case 3:
			this.name = this.type + " of lightning";
			this.dmg = this.dmg + 2;
			break;

		case 5:
			this.name = this.type + " of fire";
			this.dmg = this.dmg + 3;
			break;

		case 7:
			this.name = this.type + " of ice";
			this.dmg = this.dmg + 5;
			break;

		case 12:
			this.name = this.type + " of DOOM!";
			this.dmg = this.dmg + 10;
			break;

		default:
			this.name = this.type + " of rubbishness";
			break;

		}
	}

	// Weapon methods
	public int rollWeaponWorth(int charLVL) {
		return this.worth + this.getDice(charLVL, charLVL);
	}

	public int rollWeaponDmg(int charLVL) {
		return this.dmg + this.getDice(charLVL, 2);
	}

	public int rollAccuracy(int charLVL) {
		return this.getDice(2, charLVL);
	}

	// Dice Roller
	public int getDice(int numberOfDice, int sidesOfDie) {
		int roll = 0;
		for (int counter = 1; counter <= numberOfDice; counter++) {
			roll = roll + rand.nextInt(sidesOfDie) + 1;
		}
		return roll;
	}
}