class Menus {

	public void printIntro() {
		System.out.println("   *     *     *    *      *");
		System.out.println("*     *     *     *     *");
		System.out.println("   *     *     *    *      *");
		System.out.println("*     *     *     *     *");
		System.out.println("   *     *     *    *      *");
		System.out.println("*     *   Silent  *     *");
		System.out.println("   *      Winter    *      *");
		System.out.println("*     *    RPG    *     *");
		System.out.println("   *     *     *    *      *");
		System.out.println("*     *     *     *     *");
		System.out.println("   *     *     *    *      *");
		System.out.println("\t Alpha 0.10.1-9 SNAPSHOT");
	}

	public void printCharacterInfoPanel(Character yourCharacter, Weapons yourWeapon, Armor yourArmor) {

		System.out.println(yourCharacter.getName() + ", " + yourCharacter.getRace() + "/" + yourCharacter.getCclass());
		System.out.println("HP: " + yourCharacter.getHP() + "/" + yourCharacter.getMaxHP() + " || LVL: "
				+ yourCharacter.getLevel() + " || XP: " + yourCharacter.getXP() + "/" + yourCharacter.getRequiredXP()
				+ " || Gold: " + yourCharacter.getGold() + " || Potions: " + yourCharacter.getHPPotions()
		+ " || AC: " + (yourCharacter.getAC() + yourArmor.ac));
		System.out.println("Weapon: " + yourWeapon.name + " || Worth " + yourWeapon.worth + " || Damage: "
				+ yourWeapon.dmg + " || Accuracy: " + yourWeapon.acc);
		System.out.println("Armor: " + yourArmor.name + " || Worth " + yourArmor.worth + " || AC: " + yourArmor.ac);
	}

	public void printMainInGameMenu(Character yourCharacter) {
		System.out.println("================================================================================"); // 80!
		System.out.println("1.) Look for trouble");
		System.out.println("2.) Drink Potion (" + yourCharacter.getHPPotions() + ")");
		System.out.println("3.) Shop");
		System.out.println("================================================================================"); // 80!
	}

	public void printFightMenuSkills(Character yourCharacter, Monster monster, Classskills skills, Weapons yourWeapon, Armor yourArmor) {
		System.out.println("================================================================================"); // 80!
		System.out.println(monster.getName());
		if (monster.getStatus().equals("none")) {
		System.out.println("HP: " + monster.getHP()); // Healthbar follows
		} else {
			System.out.println("HP: " + monster.getHP() + " || Status: " + skills.statusEffectName + " (" + monster.getStatusRounds() + ")");
		}
		System.out.println("");
		System.out.println("");
		System.out.println("");
		System.out.println("");
		printCharacterInfoPanel(yourCharacter, yourWeapon, yourArmor);
		System.out.println("================================================================================"); // 80!
		System.out.println("1.) (" + skills.skill1ACD + ") " + skills.skill1Name + "| 5.) Drink potion ("
				+ yourCharacter.getHPPotions() + ")");
		System.out.println("\t + 25% chance to " + skills.statusEffectName);
		System.out.println("2.) (" + skills.skill2ACD + ") " + skills.skill2Name + "|");
		System.out.println("3.) (" + skills.skill3ACD + ") " + skills.skill3Name + "|");
		System.out.println("4.) (" + skills.skill4ACD + ") " + skills.skill4Name + "|");
		System.out.println("================================================================================"); // 80!
	}

	public void printFightPotionMenu(Character yourCharacter, Monster monster, Classskills skills, Weapons yourWeapon, Armor yourArmor) {
		System.out.println("================================================================================"); // 80!
		System.out.println(monster.getName());
		System.out.println("HP: " + monster.getHP()); // Healthbar follows
		System.out.println("");
		System.out.println("");
		System.out.println("");
		System.out.println("");
		printCharacterInfoPanel(yourCharacter, yourWeapon, yourArmor);
		System.out.println("================================================================================"); // 80!
		System.out.println("0.) Back");
		System.out.println("1.) Drink small potion (" + yourCharacter.getHPPotionsSmall() + ")");
		System.out.println("2.) Drink medium potion (" + yourCharacter.getHPPotionsMedium() + ")");
		System.out.println("================================================================================"); // 80!
	}
	
	public void printShopMenu(Shop shop, Weapons shopWeapon, Armor shopArmor) {
		System.out.println(
				"================================================================================");
		System.out.println("Welcome to the Shop");
		System.out.println("0.) Exit Shop");
		System.out.println("1.) " + shop.hpPotionSmall + ": " + shop.getPotionSmallPrice() + " Gold");
		System.out.println("2.) " + shop.hpPotionMedium + ": " + shop.getPotionMediumPrice() + " Gold");
		System.out.println("3.) " + shopWeapon.name + " (Damage: " + shopWeapon.dmg + " | Accuracy: " + shopWeapon.acc + ")" + ": " + (shopWeapon.worth + 10) + " Gold");
		System.out.println("4.) " + shopArmor.name + "(AC: " + shopArmor.ac + "): " + (shopArmor.worth + 10) + " Gold");
		System.out.println(
				"================================================================================");
	}
}