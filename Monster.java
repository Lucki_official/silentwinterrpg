// Import randomizer
import java.util.Random;
//import java.util.function.Supplier;

class Monster {

	private int xp_gain;
	private String name;
	private String type;
	private int ac;
	private int hp;
	private int damage;
	private int cr = 0;
	private int gold;
	private String status = "none";
	private int statusRounds = 0;

	/*
	 * @FunctionalInterface interface MonsterFactory { Monster createMonster(); }
	 * 
	 * private static final MonsterFactory[][] factories = { // CR 0 { () -> new
	 * Monster("Skeleton", ""), () -> new Monster("Goblin", ""), }, // CR 1 // ...
	 * };
	 * 
	 * public static createMonster(int characterLevel) { int r = 0; int cr = 0;
	 * return factories[cr][r].createMonster(); }
	 */

	// Initialize randomizer
	Random rand = new Random();

	// monster's constructor
	Monster(int charLVL) {

		// Roll out the monster's CR
		if (charLVL == 1) {
			cr = (rand.nextInt(2) - 1) + charLVL;
		}

		else if (charLVL >= 8) {
			cr = 8;
		} else {
			cr = (rand.nextInt(3) - 2 + charLVL);
		}

		// Roll out the monster of the rolled CR
		int r = rand.nextInt(3);

		switch (cr) {

		case 0: // CR 0
			switch (r) {

			case 0:
				this.name = "Skeleton";
				this.type = "Undead";
				this.ac = 16 + rand.nextInt(3) - 1;
				this.hp = rand.nextInt(8) + 1;
				this.gold = this.getDice(1, 3);
				this.damage = this.getDice(1, 6);
				break;

			case 1:
				this.name = "Goblin";
				this.type = "Goblin";
				this.ac = 15 + rand.nextInt(3) - 1;
				this.hp = rand.nextInt(10) + 1;
				this.gold = this.getDice(1, 3);
				this.damage = this.getDice(1, 4);
				break;

			case 2:
				this.name = "DAS MOP!";
				this.type = "Mopp";
				this.ac = 0;
				this.hp = 1;
				this.gold = this.getDice(2, 3);
				this.damage = this.getDice(1, 20);
				break;

			default:
				this.name = "Snow";
			}
			break;

		case 1: // CR 1
			switch (r) {

			case 0:
				this.name = "Bee";
				this.type = "Insect";
				this.ac = 13 + rand.nextInt(3) - 1;
				this.hp = this.getDice(3, 8);
				this.gold = this.getDice(1, 4);
				this.damage = this.getDice(1, 4);
				break;

			case 1:
				this.name = "Frog";
				this.type = "Animal";
				this.ac = 12 + rand.nextInt(3) - 1;
				this.hp = this.getDice(2, 8) + 6;
				this.gold = this.getDice(1, 4);
				this.damage = this.getDice(1, 6) + 2;
				break;

			case 2:
				this.name = "Spider";
				this.type = "Insect";
				this.ac = 14 + rand.nextInt(3) - 1;
				this.hp = this.getDice(3, 8) + 3;
				this.gold = this.getDice(2, 4);
				this.damage = this.getDice(1, 6);
				break;

			default:
				this.name = "Snow";
			}
			break;

		case 2: // CR 2
			switch (r) {

			case 0:
				this.name = "Boar";
				this.type = "Animal";
				this.ac = 14 + rand.nextInt(3) - 1;
				this.hp = this.getDice(2, 8) + 9;
				this.gold = this.getDice(1, 5);
				this.damage = this.getDice(1, 8) + 4;
				break;

			case 1:
				this.name = "Crab";
				this.type = "Animal";
				this.ac = 16 + rand.nextInt(3) - 1;
				this.hp = this.getDice(3, 8) + 6;
				this.gold = this.getDice(1, 5);
				this.damage = this.getDice(1, 4) + 2;

				break;

			case 2:
				this.name = "Zombie";
				this.type = "Undead";
				this.ac = 14 + rand.nextInt(3) - 1;
				this.hp = this.getDice(4, 8) + 8;
				this.gold = this.getDice(2, 5);
				this.damage = this.getDice(1, 6) + 3;
				break;

			default:
				this.name = "Snow";
			}
			break;

		case 3: // CR 3
			switch (r) {

			case 0:
				this.name = "Cockatrice";
				this.type = "Animal";
				this.ac = 15 + rand.nextInt(3) - 1;
				this.hp = this.getDice(5, 10) + 1;
				this.gold = this.getDice(2, 6);
				this.damage = this.getDice(1, 4);
				break;

			case 1:
				this.name = "Dire Wolf";
				this.type = "Animal";
				this.ac = 14 + rand.nextInt(3) - 1;
				this.hp = this.getDice(5, 8) + 15;
				this.gold = this.getDice(1, 6);
				this.damage = this.getDice(1, 8) + 6;
				break;

			case 2:
				this.name = "Ogre";
				this.type = "Humanoid";
				this.ac = 17 + rand.nextInt(3) - 1;
				this.hp = this.getDice(4, 8) + 15;
				this.gold = this.getDice(1, 6);
				this.damage = this.getDice(2, 8) + 7;
				break;

			default:
				this.name = "Snow";
			}
			break;

		case 4: // CR 4
			switch (r) {

			case 0:
				this.name = "Grizzly Bear";
				this.type = "Animal";
				this.ac = 16 + rand.nextInt(3) - 1;
				this.hp = this.getDice(5, 8) + 20;
				this.gold = this.getDice(1, 7);
				this.damage = this.getDice(1, 6) + 5;
				break;

			case 1:
				this.name = "Forest Drake";
				this.type = "Dragon";
				this.ac = 17 + rand.nextInt(3) - 1;
				this.hp = this.getDice(5, 12) + 10;
				this.gold = this.getDice(1, 7);
				this.damage = this.getDice(1, 8) + 4;
				break;

			case 2:
				this.name = "Hydra";
				this.type = "Magical Animal";
				this.ac = 15 + rand.nextInt(3) - 1;
				this.hp = this.getDice(5, 10) + 20;
				this.gold = this.getDice(2, 7);
				this.damage = this.getDice(1, 8) + 3;
				break;

			default:
				this.name = "Snow";
			}
			break;

		case 5: // CR 5
			switch (r) {

			case 0:
				this.name = "Basilisk";
				this.type = "Magical Animal";
				this.ac = 17 + rand.nextInt(3) - 1;
				this.hp = this.getDice(7, 10) + 14;
				this.gold = this.getDice(1, 8);
				this.damage = this.getDice(1, 8) + 4;
				break;

			case 1:
				this.name = "Ghul";
				this.type = "Undead";
				this.ac = 18 + rand.nextInt(3) - 1;
				this.hp = this.getDice(6, 8) + 24;
				this.gold = this.getDice(1, 8);
				this.damage = this.getDice(1, 6) + 6;
				break;

			case 2:
				this.name = "Manticore";
				this.type = "Magical Animal";
				this.ac = 17 + rand.nextInt(3) - 1;
				this.hp = this.getDice(6, 10) + 24;
				this.gold = this.getDice(2, 8);
				this.damage = this.getDice(1, 8) + 5;
				break;

			default:
				this.name = "Snow";
			}
			break;
		case 6: // CR 6
			switch (r) {

			case 0:
				this.name = "Wyvern";
				this.type = "Dragon";
				this.ac = 19 + rand.nextInt(3) - 1;
				this.hp = this.getDice(7, 12) + 28;
				this.gold = this.getDice(1, 9);
				this.damage = this.getDice(2, 6) + 4;
				break;

			case 1:
				this.name = "Trollscher";
				this.type = "Troll";
				this.ac = 19 + rand.nextInt(3) - 1;
				this.hp = this.getDice(7, 8) + 49;
				this.gold = this.getDice(2, 9);
				this.damage = this.getDice(1, 8) + 7;
				break;

			case 2:
				this.name = "Sea Drake";
				this.type = "Dragon";
				this.ac = 19 + rand.nextInt(3) - 1;
				this.hp = this.getDice(7, 12) + 28;
				this.gold = this.getDice(1, 9);
				this.damage = this.getDice(1, 8) + 6;
				break;

			default:
				this.name = "Snow";
			}
			break;
		case 7: // CR 7
			switch (r) {

			case 0:
				this.name = "Dire Bear";
				this.type = "Animal";
				this.ac = 18 + rand.nextInt(3) - 1;
				this.hp = this.getDice(10, 8) + 50;
				this.gold = this.getDice(1, 10);
				this.damage = this.getDice(1, 6) + 7;
				break;

			case 1:
				this.name = "Naga";
				this.type = "Aquatic";
				this.ac = 20 + rand.nextInt(3) - 1;
				this.hp = this.getDice(8, 8) + 40;
				this.gold = this.getDice(2, 10);
				this.damage = this.getDice(2, 6) + 5;
				break;

			case 2:
				this.name = "Shadow Demon";
				this.type = "Demon";
				this.ac = 18 + rand.nextInt(3) - 1;
				this.hp = this.getDice(7, 10) + 21;
				this.gold = this.getDice(1, 10);
				this.damage = this.getDice(1, 8) + this.getDice(1, 6);
				break;

			default:
				this.name = "Snow";
			}
			break;

		case 8: // CR 8
			switch (r) {

			case 0:
				this.name = "Nephilim";
				this.type = "Outsider";
				this.ac = 22 + rand.nextInt(3) - 1;
				this.hp = this.getDice(11, 10) + 44;
				this.gold = this.getDice(2, 11);
				this.damage = this.getDice(2, 6) + 11;
				break;

			case 1:
				this.name = "Bone Golem";
				this.type = "Costruct";
				this.ac = 21 + rand.nextInt(3) - 1;
				this.hp = this.getDice(11, 10) + 30;
				this.gold = this.getDice(1, 11);
				this.damage = this.getDice(1, 8) + 4;
				break;

			case 2:
				this.name = "Gorgon";
				this.type = "Magical Animal";
				this.ac = 20 + rand.nextInt(3) - 1;
				this.hp = this.getDice(8, 10) + 56;
				this.gold = this.getDice(1, 11);
				this.damage = this.getDice(2, 8) + 7;
				break;

			default:
				this.name = "Snow";
			}
			break;

		default: // default CR
			this.name = "Snow";
			break;
		}

		this.xp_gain = this.cr + (rand.nextInt(3) + 1);

	}

	// monster's methods

	// Dice Roller
	public int getDice(int numberOfDice, int sidesOfDie) {
		int roll = 0;
		for (int counter = 1; counter <= numberOfDice; counter++) {
			roll = roll + rand.nextInt(sidesOfDie) + 1;
		}
		return roll;
	}

	// Output of monster's name
	public String getName() {
		return this.name;
	}

	// Output of monster's type (testing purpose)
	public String getType() {
		return this.type;
	}

	// Amount of XP the monster is worth
	public int getXP() {
		return this.xp_gain;
	}

	// Output of monster's hitpoints
	public int getHP() {
		return this.hp;
	}

	// Output of monster's gold worth
	public int getGold() {
		return this.gold;
	}

	// Amount of damage the monster takes
	public void lose_hp(int charDmg) {
		this.hp = this.hp - charDmg;
	}

	// Calculation of how much damage the monster deals
	public int damage() {
		return this.damage;
	}

	// Output of monster's CR
	public int getCR() {
		return this.cr;
	}

	// Check if monster is dead
	public boolean is_dead() {
		if (this.hp <= 0) {
			return true;
		} else {
			return false;
		}
	}
	
	// Check if monster has a certain status
	public String getStatus() {
		return this.status;
	}
	
	public void sufferStatus(String newStatus) {
		this.status = newStatus;
	}
	
	public void addStatusRounds() {
		this.statusRounds++;
	}
	
	public void removeStatusRounds() {
		if (this.statusRounds > 0) {
		this.statusRounds--;
		}
		if (this.statusRounds == 0) {
			this.sufferStatus("none");
		}
	}
	public int getStatusRounds() {
		return this.statusRounds;
	}

	// Check if monster gets hit (AC)
	public boolean is_hit(int hit) {
		if (hit <= this.ac) {
			return false;
		} else {
			return true;
		}
	}

	// Monster's Initiative
	public int getInitiative() {
		return this.getDice(1, 20);
	}
}