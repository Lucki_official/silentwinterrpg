// Import randomizer
import java.util.Random;

class Character {

	private int xp;
	private String name;
	private String cclass;
	private String race;
	private int lvl = 1;
	private int ac;
	private int maxHP;
	private int hp;
	private int damage;
	private int hpPotionSmall = 0;
	private int hpPotionMedium = 1;
	private int hpPotions;
	private int slainMonstersCounter;
	private int neededXP = 4;
	private int gold = 0;
	private int initiative;

	// character's constructor

	Character(int race_input, int class_input, String name_input) {

		int ci = class_input;
		int ri = race_input;
		name = name_input;

		// Races
		switch (ri) {

		case 1:
			this.race = "Human";
			this.ac = 4;
			this.initiative = this.initiative + 0;
			break;

		case 2:
			this.race = "Elf";
			this.ac = 3;
			this.initiative = this.initiative + +2;
			break;

		case 3:
			this.race = "Half-Orc";
			this.ac = 6;
			this.initiative = this.initiative + -1;
			break;

		case 4:
			this.race = "Dwarf";
			this.ac = 8;
			this.initiative = this.initiative + +1;
			break;
			
		default:
			this.race = "Snow";
			this.ac = 1;
			break;

		}

		// Classes
		switch (ci) {

		case 1:
			this.cclass = "Fighter";
			this.maxHP = 40;
			this.hp = this.maxHP;
			this.initiative = this.initiative + 1;
			break;

		case 2:
			this.cclass = "Mage";
			this.maxHP = 24;
			this.hp = this.maxHP;
			this.initiative = this.initiative - 2;
			break;

		case 3:
			this.cclass = "Rogue";
			this.maxHP = 32;
			this.hp = this.maxHP;
			this.initiative = this.initiative + 3;
			break;

		case 4:
			this.cclass = "Ranger";
			this.maxHP = 40;
			this.hp = this.maxHP;
			this.initiative = this.initiative + 2;
			break;

		default:
			this.cclass = "Snow";
			this.maxHP = 1;
			this.hp = this.maxHP;
			break;
		}

	}

	// character's methods

	// Output of character's level
	public int getLevel() {
		return this.lvl;
	}

	// Output of character's name
	public String getName() {
		return this.name;
	}

	// Output of character's race
	public String getRace() {
		return this.race;
	}

	// Output of character's class
	public String getCclass() {
		return this.cclass;
	}

	// Output of character's total potions
	public int getHPPotions() {
		this.hpPotions = this.hpPotionSmall + this.hpPotionMedium;
		return this.hpPotions;
	}

	// Output of character's small potions
	public int getHPPotionsSmall() {
		return this.hpPotionSmall;
	}

	// Output of character's medium potions
	public int getHPPotionsMedium() {
		return this.hpPotionMedium;
	}

	// Restore HP with HP Potion Small
	public void healWithHOPPotionSmall() {
		if (this.hp + 5 > this.maxHP) {
			this.hp = this.maxHP;
			this.hpPotionSmall--;
		} else {
			this.hp = this.hp + 5;
			this.hpPotionSmall--;
		}
	}

	// Restore HP with HP Potion Medium
	public void healWithHPPOtionMedium() {
		if (this.hp + 10 > this.maxHP) {
			this.hp = this.maxHP;
			this.hpPotionMedium--;
		} else {
			this.hp = this.hp + 10;
			this.hpPotionMedium--;
		}
	}

	// Add SmallHP Potion to inventory
	public void addHPPotionSmall() {
		this.hpPotionSmall++;
	}

	// Add Medium HP Potion to inventory
	public void addHPPotionMedium() {
		this.hpPotionMedium++;
	}

	// Output of character's earned XP
	public int getXP() {
		return this.xp;
	}

	// Gain XP after the fight and LVL up if enough XP were earned
	public void gainXP(int monster_xp) {
		this.xp = this.xp + monster_xp;
		if (this.xp >= this.neededXP) {
			this.lvl++;
			this.xp = this.xp - this.neededXP;
			this.neededXP = this.neededXP * 2;
			this.hpPotionMedium++;
			this.maxHP = this.maxHP + 3;
			this.hp = this.maxHP;
		}

	}

	// Output required XP to LVL up
	public int getRequiredXP() {
		return this.neededXP;
	}

	// Output of character's gold
	public int getGold() {
		return this.gold;
	}

	// Adds gold to the character after slaying a monster
	public void addGold(int monster_gold) {
		this.gold = this.gold + monster_gold;
	}

	// Subtracts gold from character after buying something
	public void loseGold(int buyPrice) {
		this.gold = this.gold - buyPrice;
	}

	// Output of character's AC
	public int getAC() {
		return this.ac;
	}

	// Output of character's hitpoints
	public int getHP() {
		return this.hp;
	}

	// Output of character's max hitpoints
	public int getMaxHP() {
		return this.maxHP;
	}

	// character loses hitpoints due to monster attacking him
	public void lose_hp(int monster_dmg) {
		this.hp = this.hp - monster_dmg;
	}

	// character deals damage
	public int damage() {
		Random rand = new Random();
		this.damage = this.lvl + (rand.nextInt(3) + 1);
		return this.damage;
	}

	// check if character is dead
	public boolean is_dead() {
		if (this.hp <= 0) {
			return true;
		} else {
			return false;
		}
	}

	// check if characters gets hit (AC)
	public boolean is_hit(int hit) {
		if (hit <= this.ac) {
			return false;
		} else {
			return true;
		}
	}

	// Output of number of slain monsters
	public int getMonsterCounter() {
		return this.slainMonstersCounter;
	}

	// Adding to the slain monsters counter
	public void gainMonsterCounter() {
		this.slainMonstersCounter++;
	}

	// Output Initiative
	public int getInitiative() {
		return this.initiative;
	}
}