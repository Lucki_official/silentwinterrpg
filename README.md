# Silent Winter RPG

Requirements
------------

So far you will only need a functional OpenJDK8 or higher.

Start the Game
--------------
~~~ sh
$ java -jar silentwinter.jar
~~~

or compile it yourself
~~~ sh
$ jar -cvfe silentwinter.jar silentwinter *.class
~~~

Development overview
--------------------
https://board.disroot.org/project/veevee-silent-winter/kanban

The Kanban Board is publicly available and provides an overview about the progress of the project.
