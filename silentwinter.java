
/**
 * Silent Winter RPG
 * v 0.10 - Alpha
 *
 * @author VeeVee
 * @presented by JIL Entertainment
 * @supported by Lucki
 *
 * @AlphaTesters: comrad, BOHverkill, ChemBro, Tokki, buscher, OptimusBrem, x2s aka BMO, qwertfisch
 * @BigThanks to the Holarse Linux Gaming Community
 *
 */
// import java.util.InputMismatchException;
import java.util.Scanner;

public class silentwinter {

	public static void main(String[] args) {
		Scanner scan = new Scanner(System.in);
		Menus menu = new Menus();
		String OS = System.getProperty("os.name").toLowerCase();

		clearScreen(OS);
		menu.printIntro();
		scan.nextLine();

		int mainMenuAction = 999;
		boolean errorcatcher = true; // false if no errors

		// NEW CHARACTER CREATIOR BEGINNING
		int crace = 0;
		int cclass = 0;
		String cname;

		clearScreen(OS);

		// RACE CHOOSER BEGINNING
		do {
			try {
				System.out.println("Choose your race:");
				System.out.println("1.) Human || 2.) Elf || 3.) Half-Orc || 4.) Dwarf");
				crace = scan.nextInt();
				scan.nextLine();
				if (crace > 4 || crace < 1) { // Don't forget to change this if we add more races
					clearScreen(OS);
					System.out.println("Invalid Input! Try again");
				}
				errorcatcher = false;
			} catch (Exception e) {
				clearScreen(OS);
				System.out.println("Invalid Input! Try again");
				scan.nextLine();
			}
		} while (errorcatcher == true || (crace > 4 || crace < 1)); // Don't forget to change this if we add more races
		// RACE CHOOSER END
		clearScreen(OS);
		// CLASS CHOOSER BEGINNING
		do {
			try {
				System.out.println("Choose your class:");
				System.out.println("1.) Fighter || 2.) Mage || 3.) Rogue || 4.) Ranger");
				cclass = scan.nextInt();
				scan.nextLine();
				if (cclass > 4 || cclass < 1) { // Don't forget to change this if we add more classes
					clearScreen(OS);
					System.out.println("Invalid Input! Try again");
				}
				errorcatcher = false;
			} catch (Exception e) {
				clearScreen(OS);
				System.out.println("Invalid Input! Try again");
				scan.nextLine();
			}
		} while (errorcatcher == true || (cclass > 4 || cclass < 1)); // Don't forget to change this if we add more
		// classes
		// CLASS CHOOSER END
		clearScreen(OS);

		System.out.println("Your name (32 Characters max) :");
		cname = scan.nextLine();
		if (cname.length() > 32) {
			cname = cname.substring(0, 32);
		}

		Character yourCharacter = new Character(crace, cclass, cname);
		Classskills yourSkills = new Classskills(cclass);
		Weapons yourWeapon = new Weapons();
		Armor yourArmor = new Armor();

		while (yourWeapon.usableClass != yourCharacter.getCclass()) {
			yourWeapon = new Weapons();
		}
		while (yourArmor.usableClass != yourCharacter.getCclass()) {
			yourArmor = new Armor();
		}

		Shop shop = new Shop();
		Weapons shopWeapon = new Weapons();
		Armor shopArmor = new Armor();

		// NEW CHARACTER CREATIOR END
		clearScreen(OS);
		// GAME BEGINNING
		errorcatcher = true;
		while (mainMenuAction != 0) {
			while (errorcatcher == true) {
				if (mainMenuAction == 999) {
					menu.printCharacterInfoPanel(yourCharacter, yourWeapon, yourArmor);

					menu.printMainInGameMenu(yourCharacter);

					try {
						mainMenuAction = scan.nextInt();
						scan.nextLine();
						errorcatcher = false;
					} catch (Exception e) {
						clearScreen(OS);
						System.out.println("Invalid Input! Try again");
						scan.nextLine();
					}

				}

				switch (mainMenuAction) {

				case 1:
					Fight(yourCharacter, yourSkills, yourWeapon, yourArmor, menu, scan, shop, OS);
					mainMenuAction = 999;
					errorcatcher = true;
					int rollShopReset = yourWeapon.getDice(1, 3);
					if (rollShopReset == 3) {
						Weapons newShopWeapon = new Weapons();
						shopWeapon.name = newShopWeapon.name;
						shopWeapon.type = newShopWeapon.type;
						shopWeapon.usableClass = newShopWeapon.usableClass;
						shopWeapon.worth = newShopWeapon.rollWeaponWorth(yourCharacter.getLevel());
						shopWeapon.dmg = newShopWeapon.rollWeaponDmg(yourCharacter.getLevel());
						shopWeapon.acc = newShopWeapon.rollAccuracy(yourCharacter.getLevel());
						Armor newShopArmor = new Armor();
						shopArmor.name = newShopArmor.name;
						shopArmor.type = newShopArmor.type;
						shopArmor.usableClass = newShopArmor.usableClass;
						shopArmor.worth = newShopArmor.rollArmorWorth(yourCharacter.getLevel());
						shopArmor.ac = newShopArmor.rollArmorAC(yourCharacter.getLevel());
						System.out.println("The shop received new items");
						System.out.println("");
					}
					break;

				case 2:
					clearScreen(OS);
					int potionAction = 999;
					while (potionAction == 999) {

						menu.printCharacterInfoPanel(yourCharacter, yourWeapon, yourArmor);
						System.out.println(
								"================================================================================");
						System.out.println("0.) Back");
						System.out.println("1.) Drink small potion (" + yourCharacter.getHPPotionsSmall() + ")");
						System.out.println("2.) Drink medium potion (" + yourCharacter.getHPPotionsMedium() + ")");
						System.out.println(
								"================================================================================");

						try {
							potionAction = scan.nextInt();
							scan.nextLine();
						} catch (Exception e) {
							clearScreen(OS);
							System.out.println("Invalid Input! Try again");
							scan.nextLine();
						}

						switch (potionAction) {

						case 0:
							clearScreen(OS);
							mainMenuAction = 999;
							break;

						case 1:
							if (yourCharacter.getHPPotionsSmall() == 0) {
								clearScreen(OS);
								System.out.println("Invalid option");
							} else {
								if (yourCharacter.getHP() + 5 < yourCharacter.getMaxHP()) {
									clearScreen(OS);
									System.out.println("You've healed for 5 HP");
								} else {
									clearScreen(OS);
									System.out.println("You've healed back up to your maximum HP");
								}
								yourCharacter.healWithHOPPotionSmall();
								potionAction = 999;
							}
							break;

						case 2:
							if (yourCharacter.getHPPotionsMedium() == 0) {
								System.out.println("Invalid option");
							} else {
								if (yourCharacter.getHP() + 10 < yourCharacter.getMaxHP()) {
									clearScreen(OS);
									System.out.println("You've healed for 10 HP");
								} else {
									clearScreen(OS);
									System.out.println("You've healed back up to your maximum HP");
								}
								yourCharacter.healWithHPPOtionMedium();
								potionAction = 999;
							}
							break;

						default:
							clearScreen(OS);
							System.out.println("Invalid Option");
							potionAction = 999;
							break;
						}
					}

					errorcatcher = true;
					break;

				case 3:
					clearScreen(OS);
					int shopAction = 999;
					while (shopAction == 999) {
						menu.printCharacterInfoPanel(yourCharacter, yourWeapon, yourArmor);
						menu.printShopMenu(shop, shopWeapon, shopArmor);

						try {
							shopAction = scan.nextInt();
							scan.nextLine();
						} catch (Exception e) {
							clearScreen(OS);
							System.out.println("Invalid Input! Try again");
							scan.nextLine();
						}

						switch (shopAction) {

						case 0:
							clearScreen(OS);
							System.out.println("You've left the Shop");
							System.out.println("");
							mainMenuAction = 999;
							break;

						case 1:
							if (yourCharacter.getGold() >= shop.getPotionSmallPrice()) {
								yourCharacter.loseGold(shop.getPotionSmallPrice());
								yourCharacter.addHPPotionSmall();
								clearScreen(OS);
								System.out.println("You've bought a small potion");
								System.out.println("");
								shopAction = 999;
							} else {
								clearScreen(OS);
								System.out.println("Not enough Gold");
								System.out.println("");
								shopAction = 999;
							}
							break;
						case 2:
							if (yourCharacter.getGold() >= shop.getPotionMediumPrice()) {
								yourCharacter.loseGold(shop.getPotionMediumPrice());
								yourCharacter.addHPPotionMedium();
								clearScreen(OS);
								System.out.println("You've bought a medium potion");
								System.out.println("");
								shopAction = 999;
							} else {
								clearScreen(OS);
								System.out.println("Not enough Gold");
								System.out.println("");
								shopAction = 999;
							}
							break;

						case 3:
							if (yourCharacter.getCclass().equals(shopWeapon.usableClass) == false
									|| yourCharacter.getGold() + yourWeapon.worth < (shopWeapon.worth + 10)) {
								System.out.println("\033[H\033[2J");
								if (yourCharacter.getCclass().equals(shopWeapon.usableClass) == false) {
									System.out.println("You can't use this");
								} else {
									System.out.println("Not enough Gold");
								}
								System.out.println("");
								shopAction = 999;
							} else {
								System.out.println("\033[H\033[2J");
								System.out.println("You have bought " + shopWeapon.name + " for "
										+ (shopWeapon.worth + 10) + " Gold");
								System.out.println("You've sold your old weapon " + yourWeapon.name + " for "
										+ yourWeapon.worth + " Gold");
								System.out.println("");
								yourCharacter.addGold(yourWeapon.worth);
								yourCharacter.loseGold(shopWeapon.worth + 10);
								yourWeapon.name = shopWeapon.name;
								yourWeapon.worth = shopWeapon.worth;
								yourWeapon.dmg = shopWeapon.dmg;
								yourWeapon.acc = shopWeapon.acc;
								Weapons newShopWeapon = new Weapons();
								shopWeapon.name = newShopWeapon.name;
								shopWeapon.type = newShopWeapon.type;
								shopWeapon.usableClass = newShopWeapon.usableClass;
								shopWeapon.worth = newShopWeapon.rollWeaponWorth(yourCharacter.getLevel());
								shopWeapon.dmg = newShopWeapon.rollWeaponDmg(yourCharacter.getLevel());
								shopWeapon.acc = newShopWeapon.rollAccuracy(yourCharacter.getLevel());
								shopAction = 999;
							}
							break;

						case 4:
							if (yourCharacter.getCclass().equals(shopArmor.usableClass) == false
									|| yourCharacter.getGold() + yourArmor.worth < (shopArmor.worth + 10)) {
								System.out.println("\033[H\033[2J");
								if (yourCharacter.getCclass().equals(shopArmor.usableClass) == false) {
									System.out.println("You can't wear this");
								} else {
									System.out.println("Not enough Gold");
								}
								System.out.println("");
								shopAction = 999;
							} else {
								System.out.println("\033[H\033[2J");
								System.out.println("You have bought " + shopArmor.name + " for "
										+ (shopArmor.worth + 10) + " Gold");
								System.out.println("You've sold your old armor " + yourArmor.name + " for "
										+ yourArmor.worth + " Gold");
								System.out.println("");
								yourCharacter.addGold(yourArmor.worth);
								yourCharacter.loseGold(shopArmor.worth + 10);
								yourArmor.name = shopArmor.name;
								yourArmor.worth = shopArmor.worth;
								yourArmor.ac = shopArmor.ac;
								Armor newShopArmor = new Armor();
								shopArmor.name = newShopArmor.name;
								shopArmor.type = newShopArmor.type;
								shopArmor.usableClass = newShopArmor.usableClass;
								shopArmor.worth = newShopArmor.rollArmorWorth(yourCharacter.getLevel());
								shopArmor.ac = newShopArmor.rollArmorAC(yourCharacter.getLevel());
								shopAction = 999;
							}
							break;

						default:
							clearScreen(OS);
							System.out.println("Not a valid option");
							System.out.println("");
							shopAction = 999;
							break;
						}
					}

				case 999:
					mainMenuAction = 999;
					errorcatcher = true;
					break;

				default:
					clearScreen(OS);
					System.out.println("Not a valid option");
					mainMenuAction = 999;
					errorcatcher = true;
					break;
				}
			}
			// GAME END
			scan.close();
		}

	}

	private static void Fight(Character yCharacter, Classskills ySkills, Weapons yWeapon, Armor yArmor, Menus menu,
			Scanner scan, Shop shop, String OS) {
		int damage;
		int action = 0;
		int fightMenuAction = 999;
		int turnCounter = 1;
		Monster monster = new Monster(yCharacter.getLevel());
		int initiative = monster.getInitiative() - (monster.getDice(1, 20) + yCharacter.getInitiative());

		clearScreen(OS);
		while (yCharacter.is_dead() == false && monster.is_dead() == false) {
			if (initiative >= 0) { // If the monster has higher initiative
				System.out.println("-=TURN " + turnCounter + "=-");
				if (monster.getStatus().equals("burn")) {
					int burnDmg = ySkills.statusEffectDmg;
					monster.lose_hp(burnDmg);
					System.out.println(monster.getName() + " burns and takes " + burnDmg + " damage");
					// Check if monster has died from damage
				} else if (monster.getStatus().equals("poison")) {
					int poisonDmg = ySkills.statusEffectDmg;
					monster.lose_hp(poisonDmg);
					System.out.println(monster.getName() + " is poisoned and takes " + poisonDmg + " damage");
				}
				if (!"stun".equals(monster.getStatus()) && monster.is_dead() == false) {
					int tryToHit = monster.getDice(1, 20) - yArmor.ac + monster.getCR();
					int printTryToHitRoll = tryToHit + yArmor.ac;
					System.out.println(monster.getName() + " tries to hit you (" + printTryToHitRoll + ") against ("
							+ (yCharacter.getAC() + yArmor.ac) + ")");
					// check if character gets hit
					if (yCharacter.is_hit(tryToHit) == true) {
						damage = monster.damage();
						yCharacter.lose_hp(damage);
						System.out.println(monster.getName() + " hits you and deals " + damage + " damage to you");
						if (yCharacter.is_dead() == true) {
							System.out.println("");
							menu.printCharacterInfoPanel(yCharacter, yWeapon, yArmor);
							System.out.println("You are dead. You've slain " + yCharacter.getMonsterCounter()
									+ " Monsters and became LVL " + yCharacter.getLevel() + ". Try better ;)");
							System.exit(0);
						}
					} else {
						System.out.println(monster.getName() + " misses you");
					}
				}
				if ("stun".equals(monster.getStatus()) && monster.is_dead() == false) {
					System.out.println(monster.getName() + " is stunned (" + monster.getStatusRounds());
				}
				monster.removeStatusRounds();
				ySkills.decreaseCD();
			}
			if (monster.is_dead() == true) {
				monsterIsDead(monster, yCharacter, yWeapon, yArmor, OS, scan, turnCounter);
			}

			fightMenuAction = 999;
			while (fightMenuAction == 999 && monster.is_dead() == false) {
				do {
					try {
						menu.printFightMenuSkills(yCharacter, monster, ySkills, yWeapon, yArmor);
						action = scan.nextInt();
						scan.nextLine();
					} catch (Exception e) {
						clearScreen(OS);
						System.out.println("Invalid Input! Try again");
						scan.nextLine();
					}
					if (action < 1 || action > 5) {
						clearScreen(OS);
						System.out.println("Invalid Input! Try again");
					}
				} while (action < 1 || action > 5);
				switch (action) {

				case 1:
					clearScreen(OS);
					System.out.println("-=TURN " + turnCounter + "=-");
					int tryToHit = monster.getDice(1, 20) + yCharacter.getLevel() + yWeapon.acc;
					if (monster.getStatus().equals("mark")) {
						tryToHit = tryToHit + yCharacter.getLevel();
					}
					if (monster.is_hit(tryToHit) == true) {
						damage = ySkills.skill1Dmg + yWeapon.dmg;
						monster.lose_hp(damage);
						System.out.println("You hit " + monster.getName() + " and deal " + damage + " damage to it");

						// Check if statuseffect will be implied
						if (monster.getDice(1, 20) >= 16) {
							monster.sufferStatus(ySkills.statusEffectName);
							if (ySkills.statusEffectName.equals("stun")) {
								int tempRounds = monster.getDice(1, 3);
								for (int i = 1; i <= tempRounds; i++)
									monster.addStatusRounds();
								System.out.println(
										"You have stunned " + monster.getName() + " for " + tempRounds + " rounds");
							} else if (ySkills.statusEffectName.equals("burn")) {
								int tempRounds = monster.getDice(1, 4);
								for (int i = 1; i <= tempRounds; i++)
									monster.addStatusRounds();
								System.out.println("You have set " + monster.getName() + " on fire. It will burn for "
										+ tempRounds + " rounds");
							} else if (ySkills.statusEffectName.equals("poison")) {
								for (int i = 1; i <= 50; i++)
									monster.addStatusRounds();
								System.out.println("You have poisoned " + monster.getName());
							} else if (ySkills.statusEffectName.equals("mark")) {
								int tempRounds = monster.getDice(1, 4);
								for (int i = 1; i <= tempRounds; i++)
									monster.addStatusRounds();
								System.out.println(
										"You have marked " + monster.getName() + " for " + tempRounds + " rounds");
							}
						} else {
							System.out.println(monster.getName() + " resisted your " + ySkills.statusEffectName);
						}

					} else {
						System.out.println("You've missed " + monster.getName());
					}

					// If the monster dies
					if (monster.is_dead() == true) {
						monsterIsDead(monster, yCharacter, yWeapon, yArmor, OS, scan, turnCounter);
					}

					fightMenuAction = 0;
					turnCounter++;
					break;

				case 2:
					clearScreen(OS);
					System.out.println("-=TURN " + turnCounter + "=-");
					if (ySkills.skill2ACD == 0) {
						int tryToHit2 = monster.getDice(1, 20) + yCharacter.getLevel() + yWeapon.acc;
						if (monster.getStatus().equals("mark")) {
							tryToHit2 = tryToHit2 + yCharacter.getLevel();
						}
						if (monster.is_hit(tryToHit2) == true) {
							damage = ySkills.skill2Dmg + yWeapon.dmg;
							monster.lose_hp(damage);
							System.out
									.println("You hit " + monster.getName() + " and deal " + damage + " damage to it");
							// If the monster dies
							if (monster.is_dead() == true) {
								monsterIsDead(monster, yCharacter, yWeapon, yArmor, OS, scan, turnCounter);
							}
							// If you die
						} else {
							System.out.println("You've missed " + monster.getName());
						}
						ySkills.increaseCDSkill2();
						fightMenuAction = 0;
						turnCounter++;
					} else {
						System.out.println("Your skill is on cooldown");
						fightMenuAction = 999;
					}
					break;

				case 3:
					clearScreen(OS);
					System.out.println("-=TURN " + turnCounter + "=-");
					if (ySkills.skill3ACD == 0) {
						int tryToHit3 = monster.getDice(1, 20) + yCharacter.getLevel() + yWeapon.acc;
						if (monster.getStatus().equals("mark")) {
							tryToHit3 = tryToHit3 + yCharacter.getLevel();
						}
						if (monster.is_hit(tryToHit3) == true) {
							damage = ySkills.skill3Dmg + yWeapon.dmg;
							monster.lose_hp(damage);
							System.out
									.println("You hit " + monster.getName() + " and deal " + damage + " damage to it");
							// If the monster dies
							if (monster.is_dead() == true) {
								yCharacter.gainXP(monster.getXP());
								yCharacter.addGold(monster.getGold());
								yCharacter.gainMonsterCounter();
								System.out.println("You have slain " + monster.getName() + " after " + turnCounter
										+ " turns. You get " + monster.getXP() + " XP and " + monster.getGold()
										+ " Gold");
								System.out.println("");
								int weaponDrop = monster.getDice(1, 3);
								int armorDrop = monster.getDice(1, 3);
								if (weaponDrop == 2) {
									weaponLoot(yCharacter, yWeapon, monster, scan, OS);
								}
								if (armorDrop == 1) {
									armorLoot(yCharacter, yArmor, monster, scan, OS);
								}
							}
							// If you die
						} else {
							System.out.println("You've missed " + monster.getName());
						}
						ySkills.increaseCDSkill3();
						fightMenuAction = 0;
						turnCounter++;
					} else {
						System.out.println("Your skill is on cooldown");
						fightMenuAction = 999;
					}
					break;

				case 4:
					clearScreen(OS);
					System.out.println("-=TURN " + turnCounter + "=-");
					if (ySkills.skill4ACD == 0) {
						int tryToHit4 = monster.getDice(1, 20) + yCharacter.getLevel() + yWeapon.acc;
						if (monster.getStatus().equals("mark")) {
							tryToHit4 = tryToHit4 + yCharacter.getLevel();
						}
						if (monster.is_hit(tryToHit4) == true) {
							damage = ySkills.skill4Dmg + yWeapon.dmg;
							monster.lose_hp(damage);
							System.out
									.println("You hit " + monster.getName() + " and deal " + damage + " damage to it");
							// If the monster dies
							if (monster.is_dead() == true) {
								monsterIsDead(monster, yCharacter, yWeapon, yArmor, OS, scan, turnCounter);
							}
							// If you die
						} else {
							System.out.println("You've missed " + monster.getName());
						}
						ySkills.increaseCDSkill4();
						fightMenuAction = 0;
						turnCounter++;
					} else {
						System.out.println("Your skill is on cooldown");
						fightMenuAction = 999;
					}
					break;

				case 5:
					if (yCharacter.getHPPotions() >= 1) {
						int potionAction = 999;
						clearScreen(OS);
						while (potionAction == 999) {
							menu.printFightPotionMenu(yCharacter, monster, ySkills, yWeapon, yArmor);

							try {
								potionAction = scan.nextInt();
								scan.nextLine();
							} catch (Exception e) {
								clearScreen(OS);
								System.out.println("Invalid Input! Try again");
								scan.nextLine();
							}

							switch (potionAction) {

							case 0:
								clearScreen(OS);
								fightMenuAction = 999;
								break;

							case 1:
								if (yCharacter.getHPPotionsSmall() == 0) {
									clearScreen(OS);
									System.out.println("You have no small potion to drink");
								} else {
									if (yCharacter.getHP() + 5 < yCharacter.getMaxHP()) {
										clearScreen(OS);
										System.out.println("You've healed for 5 HP");
									} else {
										clearScreen(OS);
										System.out.println("You've healed back up to your maximum HP");
									}
									yCharacter.healWithHOPPotionSmall();
									potionAction = 999;
								}
								break;

							case 2:
								if (yCharacter.getHPPotionsMedium() == 0) {
									clearScreen(OS);
									System.out.println("You have no medium potion to drink");
								} else {
									if (yCharacter.getHP() + 10 < yCharacter.getMaxHP()) {
										clearScreen(OS);
										System.out.println("You've healed for 10 HP");
									} else {
										clearScreen(OS);
										System.out.println("You've healed back up to your maximum HP");
									}
									yCharacter.healWithHPPOtionMedium();
									potionAction = 999;
								}
								break;

							default:
								clearScreen(OS);
								System.out.println("Invalid Option");
								potionAction = 999;
								break;
							}
						}

						fightMenuAction = 999;
					} else {
						clearScreen(OS);
						System.out.println("No Potion left!");
						fightMenuAction = 999;
					}
					break;

				default:
					clearScreen(OS);
					System.out.println("Not a valid option");
					fightMenuAction = 999;
					break;

				}
			}

			if (initiative < 0 && yCharacter.is_dead() == false && monster.is_dead() == false) {
				if (monster.getStatus().equals("burn")) {
					int burnDmg = ySkills.statusEffectDmg;
					monster.lose_hp(burnDmg);
					System.out.println(monster.getName() + " burns and takes " + burnDmg + " damage");
				} else if (monster.getStatus().equals("poison")) {
					int poisonDmg = ySkills.statusEffectDmg;
					monster.lose_hp(poisonDmg);
					System.out.println(monster.getName() + " is poisoned and takes " + poisonDmg + " damage");
				}

				if (!"stun".equals(monster.getStatus())) {
					int tryToHit = monster.getDice(1, 20) - yArmor.ac + monster.getCR();
					int printTryToHitRoll = tryToHit + yArmor.ac;
					System.out.println(monster.getName() + " tries to hit you (" + printTryToHitRoll + ")");
					// check if character gets hitint tryToHit = monster.getDice(1, 20) - yArmor.ac
					// + monster.getCR();
					if (yCharacter.is_hit(tryToHit) == true) {
						damage = monster.damage();
						yCharacter.lose_hp(damage);
						System.out.println(monster.getName() + " hits you (" + printTryToHitRoll + ")  and deals "
								+ damage + " damage to you");
						if (yCharacter.is_dead() == true) {
							System.out.println("");
							menu.printCharacterInfoPanel(yCharacter, yWeapon, yArmor);
							System.out.println("You are dead. You've slain " + yCharacter.getMonsterCounter()
									+ " Monsters and became LVL " + yCharacter.getLevel() + ". Try better ;)");
							System.exit(0);
						}
					} else {
						System.out.println(monster.getName() + " misses you");
					}
				} else {
					System.out.println(monster.getName() + " is stunned");
				}
				monster.removeStatusRounds();
				ySkills.decreaseCD();
			}
		}
		ySkills.resetAllCD(); // Reset all the CD's after the fight

	}

	private static void weaponLoot(Character yCharacter, Weapons yWeapon, Monster monster, Scanner scan, String OS) {

		int chooseWeaponLoot = 0;
		Weapons tempweapon = new Weapons();
		tempweapon.worth = tempweapon.rollWeaponWorth(yCharacter.getLevel());
		tempweapon.dmg = tempweapon.rollWeaponDmg(yCharacter.getLevel());
		tempweapon.acc = tempweapon.rollAccuracy(yCharacter.getLevel());
		if (tempweapon.usableClass.equals(yCharacter.getCclass())) {
			do {
				try {
					System.out.println("");
					System.out.println(monster.getName() + " dropped " + tempweapon.name);
					System.out.println("Do you want to use your new weapon and sell your old weapon,");
					System.out.println("or do you want to use your old weapon and sell the new weapon?");
					System.out.println();
					System.out.println("Old weapon: " + yWeapon.name + " | Worth: " + yWeapon.worth + " | Damage: "
							+ yWeapon.dmg + " | Accuracy: " + yWeapon.acc);
					System.out.println();
					System.out.println("New weapon: " + tempweapon.name + " | Worth " + tempweapon.worth + " | Damage: "
							+ tempweapon.dmg + " | Accuracy: " + tempweapon.acc);
					System.out.println("");
					System.out.println("");
					System.out.println("1.) Keep old weapon");
					System.out.println("2.) Use new weapon");
					chooseWeaponLoot = scan.nextInt();
					scan.nextLine();
					clearScreen(OS);
				} catch (Exception e) {
					clearScreen(OS);
					System.out.println("Invalid Input! Try again");
					scan.nextLine();
				}
			} while (chooseWeaponLoot > 2 || chooseWeaponLoot < 1);

			if (chooseWeaponLoot == 1) {
				yCharacter.addGold(tempweapon.worth);
			}
			if (chooseWeaponLoot == 2) {
				yCharacter.addGold(yWeapon.worth);
				yWeapon.type = tempweapon.type;
				yWeapon.dmg = tempweapon.dmg;
				yWeapon.acc = tempweapon.acc;
				yWeapon.worth = tempweapon.worth;
				yWeapon.name = tempweapon.name;
			}
		} else {
			int tempGold;
			tempGold = tempweapon.rollWeaponWorth(yCharacter.getLevel());
			yCharacter.addGold(tempGold);
			System.out.println(monster.getName() + " dropped " + tempweapon.name);
			System.out.println("Since you can't use the weapon, you sell it for " + tempGold + " Gold");
			System.out.println("");
		}

	}

	private static void armorLoot(Character yCharacter, Armor yArmor, Monster monster, Scanner scan, String OS) {

		int chooseArmorLoot = 0;
		Armor temparmor = new Armor();
		temparmor.worth = temparmor.rollArmorWorth(yCharacter.getLevel());
		temparmor.ac = temparmor.rollArmorAC(yCharacter.getLevel());
		if (temparmor.usableClass.equals(yCharacter.getCclass())) {
			do {
				try {
					System.out.println("");
					System.out.println(monster.getName() + " dropped " + temparmor.name);
					System.out.println("Do you want to wear the new armor and sell your old armor,");
					System.out.println("or do you want to wear your old armor and sell the new armor?");
					System.out.println();
					System.out
							.println("Old armor: " + yArmor.name + " | Worth: " + yArmor.worth + " | AC: " + yArmor.ac);
					System.out.println();
					System.out.println(
							"New armor: " + temparmor.name + " | Worth: " + temparmor.worth + " | AC: " + temparmor.ac);
					System.out.println("");
					System.out.println("");
					System.out.println("1.) Keep old armor");
					System.out.println("2.) Use new armor");
					chooseArmorLoot = scan.nextInt();
					scan.nextLine();
					clearScreen(OS);
				} catch (Exception e) {
					clearScreen(OS);
					System.out.println("Invalid Input! Try again");
					scan.nextLine();
				}
			} while (chooseArmorLoot > 2 || chooseArmorLoot < 1);

			if (chooseArmorLoot == 1) {
				yCharacter.addGold(temparmor.worth);
			}
			if (chooseArmorLoot == 2) {
				yCharacter.addGold(temparmor.worth);
				yArmor.type = temparmor.type;
				yArmor.ac = temparmor.ac;
				yArmor.worth = temparmor.worth;
				yArmor.name = temparmor.name;
			}
		} else {
			int tempGold;
			tempGold = temparmor.rollArmorWorth(yCharacter.getLevel());
			yCharacter.addGold(tempGold);
			System.out.println(monster.getName() + " dropped " + temparmor.name);
			System.out.println("Since you can't use the armor, you sell it for " + tempGold + " Gold");
			System.out.println("");
		}
	}

	private static void clearScreen(String OS) {

		if (isWindows(OS)) {
			for (int i = 0; i < 50; ++i)
				System.out.println();
		} else if (isMac(OS)) {
			System.out.print("\033[H\033[2J");
		} else if (isUnix(OS)) {
			System.out.print("\033[H\033[2J");
		}
	}

	public static boolean isWindows(String OS) {

		return (OS.indexOf("win") >= 0);

	}

	public static boolean isMac(String OS) {

		return (OS.indexOf("mac") >= 0);

	}

	public static boolean isUnix(String OS) {

		return (OS.indexOf("nix") >= 0 || OS.indexOf("nux") >= 0 || OS.indexOf("aix") > 0);
	}

	public static void monsterIsDead(Monster monster, Character yCharacter, Weapons yWeapon, Armor yArmor, String OS,
			Scanner scan, int turnCounter) {

		yCharacter.gainXP(monster.getXP());
		yCharacter.addGold(monster.getGold());
		yCharacter.gainMonsterCounter();
		System.out.println("You have slain " + monster.getName() + " after " + turnCounter + " turns. You get "
				+ monster.getXP() + " XP and " + monster.getGold() + " Gold");
		System.out.println("");
		int weaponDrop = monster.getDice(1, 3);
		int armorDrop = monster.getDice(1, 3);
		if (weaponDrop == 2) {
			weaponLoot(yCharacter, yWeapon, monster, scan, OS);
		}
		if (armorDrop == 1) {
			armorLoot(yCharacter, yArmor, monster, scan, OS);
		}

	}
}