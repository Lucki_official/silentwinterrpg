class Shop {

	String hpPotionMedium = "Medium HP Potion";
	int hpPotionMediumPrice = 20;
	String hpPotionSmall = "Small HP Potion";
	int hpPotionSmallPrice = 10;
	String shopWeaponName, shopWeaponType, shopWeaponUseableClass;
	int shopWeaponDmg, shopWeaponAcc;
	String shopArmorName, shopArmorType, shopArmorUseableClass;
	int shopArmorAc;
	

	public String getPotionSmall() {
		return hpPotionSmall;
	}

	public String getPotionMedium() {
		return hpPotionMedium;
	}

	public int getPotionMediumPrice() {
		return hpPotionMediumPrice;
	}

	public int getPotionSmallPrice() {
		return this.hpPotionSmallPrice;
	}
}